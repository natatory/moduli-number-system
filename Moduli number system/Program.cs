﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moduli_number_system
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ModSystem.fromNb2Str(11, new int[] { 2, 3, 5 }));
            Console.WriteLine(ModSystem.fromNb2Str(6, new int[] { 2, 3, 4 }));
            Console.WriteLine(ModSystem.fromNb2Str(7, new int[] { 2, 3 }));
            Console.ReadLine();
        }
    }
}
