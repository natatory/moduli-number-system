﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moduli_number_system
{
    public class ModSystem
    {
        public static string fromNb2Str(int n, int[] sys)
        {
            if (sys.Aggregate((a, b) => a * b) <= n) return "Not applicable";
            int coprimeFactorCount = 0;
            for (int k = 2; k < sys.Max(); k++)
            {
                for (int i = 0; i < sys.Length; i++)
                {
                    if (sys[i] % k == 0) coprimeFactorCount++;
                    if (coprimeFactorCount > 1) return "Not applicable";
                }
                coprimeFactorCount = 0;
            }
            StringBuilder theNumberInModSystem = new StringBuilder();
            foreach (int num in sys)
            {
                theNumberInModSystem.Append("-" + n % num + "-");
            }
            return theNumberInModSystem.ToString();
        }
    }
}
